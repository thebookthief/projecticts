# A very simple Flask Hello World app for you to get started with...
from flask import Flask, redirect, url_for, render_template, request
from flask_login import LoginManager, UserMixin,  current_user, login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from pony.orm import Database, Required, Optional, db_session, select
import keygen
from forms import LoginForm
import random



app = Flask(__name__)
app.secret_key = keygen.generate()
login = LoginManager(app)
login.login_view = 'login'

db = Database()


class User(UserMixin, db.Entity):

    username = Required(str, unique=True)
    password_hash = Optional(str)
    '''
    playerscore = Required(int)
    computerscore = Required(int)
    '''

    @db_session
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    @db_session
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)




db.bind(provider='sqlite', filename='rpsdb', create_db=True)
db.generate_mapping(create_tables=True)


@login.user_loader
@db_session
def load_user(id):
    return User.get(id=int(id))


@app.route('/')
@login_required
def index():
    return render_template('start.html', NAME=current_user.username)

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(username=form.username.data)

        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))

        login_user(user)  # remember=form.remember_me.data)
        return redirect(url_for('index'))

    return render_template('login.html', title='Sign In', form=form)

@app.route('/new_user', methods=['GET', 'POST'])
@db_session
def new_user_form():
    if request.method == 'GET':
        return render_template('newuserform.html')

    elif request.method == 'POST':
        data = request.form.to_dict()

        u = User(username=data['username'])
        u.set_password(data['password'])

        return redirect(url_for('index'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')


def get_computer_move():
    options = ["rock", "paper", "scissors"]
    return options[random.randint(0,2)]

def get_winner(you_choice, computer_choice):
    winner = "computer"

    if you_choice == computer_choice:
        winner = "tie"

    if you_choice == "rock" and computer_choice == "scissors":
        winner = "you"

    if you_choice == "scissors" and computer_choice == "paper":
        winner = "player"

    if you_choice == "paper" and computer_choice == "rock":
        winner = "you"

    return winner


@app.route('/round', methods=['GET', 'POST'])
def round():
    if request.method == 'GET':
        return render_template('rounds.html')



@app.route('/rps/<choice>')
@db_session
def rps(choice):

    you_choice = choice.lower()
    computer_choice = get_computer_move()
    winner = get_winner(you_choice, computer_choice)

    return render_template("rps_game.html", winner=winner, you_choice=you_choice, computer_choice=computer_choice )


@app.route('/result')
def result():
    return render_template("results.html")


if __name__ == "__main__":
    app.run()

